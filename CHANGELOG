0.5.0
    * set the TERM environment variable to fix ROS in ssh

0.4.7
    * add '-f' option to voxl-wifi for factory setting (Soft AP, SSID: VOXL-MACADDRESS)

0.4.6
    * fix gpu utilization and add long term average to voxl-perfmon

0.4.5
    * remove chsh from postinst
    * fix bug where camera id can't be changed from -1

0.4.4
    add "none" option to voxl-configure-cameras
0.4.3
    * update bashrc PS1 prompt to show yocto

0.4.2
    * add voxl-*-bundle info as 'voxl-version' command option

0.4.1
    * cleanup file structure

0.4.0
    * update my_ros_env.sh to contain only ip addresses and customizations
    * add voxl-configure-cameras
    * add base_ros_env.sh
    * remove all the ros cmake files out to ros-indigo-build-deps

0.3.3
    * add default camera config in my_ros_env.sh for downward+stereo

0.3.2
    * replace modalai_modemutil.py updates from modem-utils

0.3.1
    * add missing cmake files for ros packages to support catkin_make on voxl
    * add build dependencies for compiling voxl-nodes on target
    * fix name to hyphenated voxl-utils to meet coding standard

0.3.0
    * add CAM_ID environment variables to ~/my_ros_env.sh for easier config
    * NOTE user will need to copy this file manually from /share/voxl_utils/my_ros_env.sh
      to ~/ if upgrading from an earlier version.

0.2.4
    * update default IP address for ROS and rename install_on_target.sh

0.2.3
    * updates for ModalAI Coding Standards

0.2.2
    * fix setup bug on clean board

0.2.1
    * bundle modalai_modemutil.py into this package (for now)
    * repo layout change

0.2.0
    * add "stress" binary
    * add -c option to profile.py to print data in csv format

0.1.2
    * Refactored from modal_utils to voxl_utils
    * Add /usr/bin/voxl_env
    * Add /usr/bin/voxl_hwscan
    * Add /usr/bin/voxl_perfmon
    * Add /usr/bin/voxl_version
    * Refactored /usr/bin/modal_wifi_setup.sh to /usr/bin/voxl_wifi, default SSID is 'voxl' now
    * Fix for profile.py divide by 0 error

0.1.1
    * Add /etc/init.d/wlan init script to configure softap IP address as 192.168.8.1
    * Add /usr/bin/profile.py for cpu load/temp measurement


0.1.0
    * initial release, includes my_ros_env.sh ROS envrionment setup
    * and .bashrc script to call it
    * also modal_wifi_set.sh script to configure station or SoftAP modes

// indentation is with spaces to allow copy/paste into debian/changelog