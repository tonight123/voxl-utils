# VOXL Utils

This repository is used to generate an IPK package of ModalAI common on-target utilities supporting setup, configuration and testing tasks.

### Installation

VOXL Utilities is included in the [VOXL Software Bundle](https://docs.modalai.com/install-software-bundles/).

### Usage

Usage instructions available at the [ModalAI Technical Documentation](https://docs.modalai.com/voxl-utils/) page.

### Build Package from source

To generate the IPK from source, perform the following:

```bash
git clone git@gitlab.com:voxl-public/voxl-utils.git
cd voxl-utils
./make_package.sh
```

To install, connect the VOXL via USB and run the following which will push the package over ADB and install on VOXL. This requires [ADB to be configured](https://docs.modalai.com/setup-adb/) on your host PC.

```bash
./install_on_voxl.sh
```
